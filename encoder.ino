const char YELL_FG = 2;
const char BLUE_PWM = 6;
const char GREE_BRAKE = 7;
const char WHIT_DIRECTION = 8;

unsigned long timeold = 0;       
unsigned int lessPulse = 0;
unsigned long pulsesTotal = 0;
unsigned char numberOfturns = 0;
bool countingTheNumberOfLaps = 0;
volatile unsigned long pulses = 0;

void counter()
{
    pulses++;
}

void setup()
{
    Serial.begin(9600);
    pinMode(BLUE_PWM, OUTPUT);
    pinMode(GREE_BRAKE, OUTPUT);
    pinMode(YELL_FG, INPUT_PULLUP);
    pinMode(WHIT_DIRECTION, OUTPUT);
    attachInterrupt(digitalPinToInterrupt(YELL_FG), counter, RISING);
    Serial.println("<----------- Options ----------->");
    Serial.println("[ s ] Start");
    Serial.println("[ t ] Stop");
    Serial.println("[ r ] Turn right");
    Serial.println("[ l ] Turn left");
    Serial.println("[ f ] Full Speed");
    Serial.println("[ g ] Speed 80%");
    Serial.println("[ h ] Speed 60%");
    Serial.println("[ j ] Speed 40%");
    Serial.println("[ k ] Speed 20%");
    Serial.println("[ o ] Speed 10%");
    Serial.println("[ p ] Speed 00%");
    Serial.println("[ 1 - 9 ] x 10 Number of turns");

    Serial.println(" ");
    Serial.println("<----------- Connections ----------->");
    Serial.println("Arduino\t\tMotor");
    Serial.println("[ 2 ]\t\tYELL - WAVE");
    Serial.println("[ 6 ]\t\tBLUE - PWM");
    Serial.println("[ 7 ]\t\tGREE - BRAKE");
    Serial.println("[ 8 ]\t\tWHIT - DIRECTION");

   

    delay(2000);
}

void loop()
{

    if ( countingTheNumberOfLaps == 1 ){
        if ( pulses >= ( pulsesTotal - lessPulse ) ) {
            // Serial.print("pulses: "); 
            // Serial.println(pulses); 
            digitalWrite(GREE_BRAKE, LOW); 
            countingTheNumberOfLaps = 0;
            // noInterrupts(); 
        }
    }
    
    if (Serial.available() > 0)
    {
        int incomingByte = Serial.read();

        switch (incomingByte)
        {
        case 0x0a:
        case 0x0d:
            break;

        case 'l':
            // Serial.println("left-izq");
            digitalWrite(WHIT_DIRECTION, LOW); 
            break;
        case 'r':
            // Serial.println("right-der");
            digitalWrite(WHIT_DIRECTION, HIGH);  
            break;
        case 's':
            // Serial.println("start"); 
            digitalWrite(GREE_BRAKE, HIGH); 
            break;
        case 't':
            // Serial.println("stop"); 
            digitalWrite(GREE_BRAKE, LOW); 
            break;
        case 'f':
            // Serial.println("full-speed"); 
            digitalWrite(BLUE_PWM, LOW); 
            break;
        case 'g':
            // Serial.println("speed-5"); 
            analogWrite(BLUE_PWM, 10); 
            break;
        case 'h':
            // Serial.println("speed-4"); 
            analogWrite(BLUE_PWM, 50); 
            break;
        case 'j':
            // Serial.println("speed-3"); 
            analogWrite(BLUE_PWM, 100); 
            break;
        case 'k':
            // Serial.println("speed-2"); 
            analogWrite(BLUE_PWM, 150); 
            break;
        case 'o':
            // Serial.println("speed-1"); 
            analogWrite(BLUE_PWM, 200); 
            break;
        case 'p':
            // Serial.println("speed-0"); 
            analogWrite(BLUE_PWM, 255); 
            break;
        
        default:
            noInterrupts();   
            // Serial.print("default: "); 
            numberOfturns = ( (incomingByte - 48) * 10 ); 

            if ( incomingByte == 48 ) 
                numberOfturns = 100;
            

            // Serial.println(numberOfturns);
            lessPulse = ( numberOfturns * 45 );
            pulsesTotal = ( 940 * numberOfturns );
         

            pulses = 0; 
            interrupts();
            digitalWrite(GREE_BRAKE, HIGH); 
            digitalWrite(BLUE_PWM, LOW); 
            countingTheNumberOfLaps = 1;
            break;
        }

    }
}